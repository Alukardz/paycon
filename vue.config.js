module.exports = {
  transpileDependencies: ['vuetify'],
  pluginOptions: {
    electronBuilder: {
      builderOptions: {
        productName: 'Pay Convert',
      },
      // externals: ['fs'],
      // experimentalNativeDepCheck: true
      // // If you are using Yarn Workspaces, you may have multiple node_modules folders
      // // List them all here so that VCP Electron Builder can find them
      // nodeModulesPath: ['./node_modules'],
      preload: 'src/preload.js',
    },
  },
}
