/* global __static */
import path from 'path'
import dotenv from 'dotenv'
import { app, protocol, BrowserWindow, screen, ipcMain } from 'electron'
import { createProtocol } from 'vue-cli-plugin-electron-builder/lib'
import installExtension, { VUEJS_DEVTOOLS } from 'electron-devtools-installer'
import apis from '@/api/apis'
import { logger } from '@/log.js'

const isDevelopment = process.env.NODE_ENV !== 'production'

dotenv.config({
  path: path.resolve(process.cwd(), process.env.NODE_ENV + '.env')
})

// Scheme must be registered before the app is ready
protocol.registerSchemesAsPrivileged([
  { scheme: 'app', privileges: { secure: true, standard: true } },
])

async function createWindow() {
  // Create the browser window.
  const displays = screen.getAllDisplays()
  const externalDisplay = displays.find((display) => {
    return display.bounds.x !== 0 || display.bounds.y !== 0
  })

  let win

  if (externalDisplay) {
    win = new BrowserWindow({
      x: externalDisplay.bounds.x + 1300,
      y: externalDisplay.bounds.y + 0,
      width: 580,
      height: 260,
      frame: false,
      title: 'Pay Convert',
      icon: path.join(__static, 'icon.png'),
      backgroundColor: '#FFF',
      webPreferences: {
        nodeIntegration: process.env.ELECTRON_NODE_INTEGRATION,
        nodeIntegrationInWorker: process.env.ELECTRON_NODE_INTEGRATION_WORKER,
        preload: path.join(__dirname, 'preload.js')
      },
    })

  } else {
    win = new BrowserWindow({
      width: 580,
      height: 260,
      frame: false,
      title: 'Pay Convert',
      icon: path.join(__static, 'icon.png'),
      webPreferences: {
        nodeIntegration: process.env.ELECTRON_NODE_INTEGRATION,
        nodeIntegrationInWorker: process.env.ELECTRON_NODE_INTEGRATION_WORKER,
        preload: path.join(__dirname, 'preload.js')
      },
    })
  }

  win.setMenu(null)

  win.on('page-title-updated', event => event.preventDefault());

  if (process.env.WEBPACK_DEV_SERVER_URL) {
    // Load the url of the dev server if in development mode
    await win.loadURL(process.env.WEBPACK_DEV_SERVER_URL)
    if (process.env.IS_TEST == 'false') win.webContents.openDevTools()
  } else {
    createProtocol('app')
    // Load the index.html when not in development
    win.loadURL('app://./index.html')
  }
}

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) createWindow()
})

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', async () => {
  if (isDevelopment && process.env.IS_TEST == 'false') {
    // Install Vue Devtoo
    try {
      await installExtension(VUEJS_DEVTOOLS)
    } catch (e) {
      console.error('Vue Devtools failed to install:', e.toString())
    }
  }
  createWindow()
})

ipcMain.on('asynchronous-message', (event, arg) => {
  if (arg !== null) {
    const conv = apis.fileProcess(arg)
    logger.log('info', `Processed: ${conv}`)
    event.reply('asynchronous-reply', conv)
  }
})

// Exit cleanly on request from parent process in development mode.
if (isDevelopment) {
  if (process.platform === 'win32') {
    process.on('message', data => {
      if (data === 'graceful-exit') {
        app.quit()
      }
    })
  } else {
    process.on('SIGTERM', () => {
      app.quit()
    })
  }
}
