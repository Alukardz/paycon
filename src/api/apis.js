import fs from 'fs';
import { conf } from '@/config/config.js'
import { logger } from '@/log.js'

var apis = {
  fileProcess (filePath) {
    let tempText = conf.newText
    try {
      const data = fs.readFileSync(filePath, 'utf-8', { mode: 0o755 })
      var text = data.toString().split('\n')
      for (let i = 1; i < text.length-1; i++) {
        var newLine = apis.replaceRange(text[i])
        tempText += newLine.concat("\n")
      }
      tempText += apis.replaceBankRange(text[0])
    } catch (err) {
      logger.warn('error', new Error(`Error caught - ${err}`))
      return 'Error en la conversión.'
    }

    const newFile = filePath.slice(0, -3).concat('csv')
    fs.writeFile(newFile, tempText, (err) => {
      if (err) {
        logger.warn('error', new Error(`Error caught - ${err}`))
        return 'Error guardando documento.'
      }
    })
    return 'Convertido exitosamente.'
  },

  replaceRange (line) {
    var newLine = ""
    for (const r of conf.ranges) {
      var selected = line.substring(r.begin, r.end)
      if (r.begin === 61) selected = Number(selected)
      newLine += `"${selected}",`
    }
    return newLine
  },

  replaceBankRange (line) {
    var bankData = ""
    for (const r of conf.bankData) {
      var selected = line.substring(r.range.begin, r.range.end)
      if (r.range.begin === 23 || r.range.begin === 13) selected = Number(selected)
      bankData += `${r.name},${selected},\n`
    }
    return bankData
  }
}

export default apis