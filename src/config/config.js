export const conf = {
  newText: 'Nombre,Cedula,##,Monto,Fecha,Impuesto,##########,\n',
  bankData: [
    {
      name: 'Entidad',
      range: {
        "begin": 2,
        "end": 13
      }
    },
    {
      name: 'Registros',
      range: {
        "begin": 13,
        "end": 20
      }
    },
    {
      name: 'Monto',
      range: {
        "begin": 23,
        "end": 40
      }
    },
    {
      name: 'Desde',
      range: {
        "begin": 40,
        "end": 48
      }
    },
    {
      name: 'Hasta',
      range: {
        "begin": 54,
        "end": 63
      }
    }
  ],
  ranges: [{
    "begin": 2,
    "end": 22
  }, {
    "begin": 36,
    "end": 47
  }, {
    "begin": 47,
    "end": 49
  }, {
    "begin": 61,
    "end": 69
  }, {
    "begin": 69,
    "end": 77
  }, {
    "begin": 87,
    "end": 102
  }, {
    "begin": 116,
    "end": 134
  }]
}