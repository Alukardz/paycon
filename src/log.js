import fs from 'fs'
import { createLogger, format, transports } from 'winston'

export const logger = createLogger({
  format: format.combine(
    format.timestamp({
      format: 'DD-MM-YYYY HH:mm:ss'
    }),
    format.errors({ stack: true }),
    format.splat(),
    format.simple()
  ),

  transports: [
    new transports.Console({
      format: format.combine(
        format.colorize()
      )
    }),

    new transports.Stream({
      stream: fs.createWriteStream('./conv.log')
    })
  ]
  
})